#!/bin/bash

# 检查Docker Compose版本
current_version=$(docker-compose --version | awk '{print $3}')
required_version="2.0"

if [ "$(printf '%s\n' "$required_version" "$current_version" | sort -V | head -n1)" != "$required_version" ]; then
  echo "当前Docker Compose版本较低，需要升级..."

    # 备份现有的docker-compose二进制文件
	  echo "备份现有的docker-compose二进制文件..."
	    sudo mv /var/packages/Docker/target/usr/bin/docker-compose /var/packages/Docker/target/usr/bin/docker-compose-backup

		  # 下载并安装最新的Docker Compose
		    echo "下载并安装最新的Docker Compose..."
			  sudo curl -SL https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64 -o /var/packages/Docker/target/usr/bin/docker-compose
			    sudo chmod +x /var/packages/Docker/target/usr/bin/docker-compose

				  echo "Docker Compose升级完成！"
				  fi

				  # 启动Docker Compose服务
				  echo "启动Docker Compose服务..."
				  sudo docker-compose up -d
